/** Copyright 2020 Jonathn Shahen */
function parse_maze(mazeStr) {
  var lines = remove_blank_lines(mazeStr.split('\n'));
  var maze = new Array(lines.length);
  var width = remove_trailing_semicolon(lines[0]).split(';').length;

  for (var i in lines) {
    maze[i] = new Array(width);
    if (line === '') con;

    var line = remove_trailing_semicolon(lines[i]).split(';');

    for (var j in line) {
      maze[i][j] = line[j].trim();
    }
  }
  return {
    maze: maze,
    num_rows: (maze.length + 1) / 2,
    num_cols: (width + 1) / 2
  };
}

function remove_blank_lines(lines) {
  var nlines = [];

  for (var l of lines) {
    if (l.trim() === '') continue;
    nlines[nlines.length] = l;
  }
  return nlines;
}

function remove_trailing_semicolon(l) {
  if (l.endsWith(';')) return l.substring(0, l.length - 2);
  return l;
}

function make_maze(
  mazeString,
  mazeCanvasId,
  w,
  h,
  padding_x,
  padding_y,
  doorPercent,
  key_font,
  door_font,
  border_thickness,
  wall_thickness,
  door_thickness
) {
  var maze_pkg = parse_maze(mazeString);
  var c = document.getElementById(mazeCanvasId);
  var ctx = c.getContext('2d');
  // START: SPECIAL TO ENSURE SHARPNESS OF LINES
  var dpi = window.devicePixelRatio;
  c.width = dpi * (maze_pkg.num_cols * w + 2 * padding_x);
  c.height = dpi * (maze_pkg.num_rows * h + 2 * padding_y);
  c.style.width = maze_pkg.num_cols * w + 2 * padding_x + 'px';
  c.style.height = maze_pkg.num_rows * h + 2 * padding_y + 'px';
  ctx.scale(dpi, dpi);
  // END: SPECIAL TO ENSURE SHARPNESS OF LINES

  console.log(maze_pkg);
  draw_maze(
    maze_pkg,
    padding_x,
    padding_y,
    w,
    h,
    ctx,
    'round',
    wall_thickness,
    'round',
    door_thickness,
    doorPercent,
    key_font,
    door_font,
    border_thickness
  );
}

/** Copyright 2020 Jonathn Shahen */
// Palette generated using http://google.github.io/palette.js/ (press F12)
var palette25 = [
  '#781c81',
  '#57197f',
  '#482585',
  '#413992',
  '#3f51a3',
  '#4067b3',
  '#447cbf',
  '#498ec1',
  '#519cb8',
  '#5ba7a6',
  '#67b092',
  '#74b67e',
  '#83ba6d',
  '#93bd60',
  '#a4be55',
  '#b4bd4c',
  '#c3ba45',
  '#d1b440',
  '#dbab3b',
  '#e29d37',
  '#e68a33',
  '#e7732f',
  '#e4592a',
  '#e03c25',
  '#d92120'
];
// Drawing Functions
function drawLineCoor(ctx, x1, y1, x2, y2, strokeStyle = 'black') {
  ctx.beginPath();
  ctx.strokeStyle = strokeStyle;
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();
}

function drawLineWH(ctx, x1, y1, w, h) {
  drawLineCoor(ctx, x1, y1, x1 + w, y1 + h);
}

function get_num(txt) {
  return parseInt(txt.substring(1));
}

function parse_cell_text(txt) {
  if (txt.startsWith('k') || txt.startsWith('w')) return '' + Math.abs(get_num(txt));
  else if (txt === 'S') return 'Start';
  else if (txt === 'G') return 'Goal';
  return txt;
}
function parse_door_text(txt) {
  if (txt.startsWith('w')) {
    return '' + parseInt(txt.substring(1));
  }
  return txt;
}

function set_colour(ctx, txt) {
  if (txt === 'S') ctx.fillStyle = 'blue';
  else if (txt === 'G') ctx.fillStyle = 'red';
  else if (txt.startsWith('k') || txt.startsWith('w'))
    ctx.fillStyle = palette25[Math.abs(get_num(txt)) % palette25.length];
  else ctx.fillStyle = 'black';
}

function draw_text(ctx, cell, x, y, font) {
  ctx.textAlign = 'center';
  ctx.textBaseline = 'middle';
  ctx.font = font;
  set_colour(ctx, cell);
  ctx.fillText(parse_cell_text(cell), x, y);
  // console.debug('Drawing Text', cell, x, y);
}

function draw_wall(ctx, x1, y1, x2, y2, lineCap, lineWidth) {
  ctx.lineCap = lineCap;
  ctx.lineWidth = lineWidth;
  drawLineCoor(ctx, x1, y1, x2, y2);
}
function draw_vert_wall(ctx, coord, lineCap, lineWidth) {
  draw_wall(ctx, coord.ur.x, coord.ur.y, coord.lr.x, coord.lr.y, lineCap, lineWidth);
}
function draw_hort_wall(ctx, coord, lineCap, lineWidth) {
  draw_wall(ctx, coord.ll.x, coord.ll.y, coord.lr.x, coord.lr.y, lineCap, lineWidth);
}

function draw_vert_door(door, ctx, coord, lineCap, lineWidth, font) {
  // overwrite the grid line
  drawLineCoor(ctx, coord.ur.x, coord.ur.y, coord.lr.x, coord.lr.y, 'white');
  // Draw broken line
  draw_wall(ctx, coord.ur.x, coord.ur.y, coord.ur.x, coord.ur.y + coord.h * coord.doorPercent, lineCap, lineWidth);
  draw_wall(ctx, coord.lr.x, coord.lr.y, coord.lr.x, coord.lr.y - coord.h * coord.doorPercent, lineCap, lineWidth);
  // text
  draw_text(ctx, door, coord.ur.x, (coord.lr.y + coord.ur.y) / 2, font);
}
function draw_hort_door(door, ctx, coord, lineCap, lineWidth, font) {
  // overwrite the grid line
  drawLineCoor(ctx, coord.ll.x, coord.ll.y, coord.lr.x, coord.lr.y, 'white');
  // Draw broken line
  draw_wall(ctx, coord.ll.x, coord.ll.y, coord.ll.x + coord.w * coord.doorPercent, coord.ll.y, lineCap, lineWidth);
  draw_wall(ctx, coord.lr.x, coord.lr.y, coord.lr.x - coord.w * coord.doorPercent, coord.lr.y, lineCap, lineWidth);
  draw_text(ctx, door, (coord.ll.x + coord.lr.x) / 2, coord.ll.y, font);
}

function get_cell_coord(x, y, w, h, i, j, doorPercent) {
  var cx = Math.floor(j / 2);
  var cy = Math.floor(i / 2);
  return {
    cx: cx, // Cell X
    cy: cy, // Cell Y
    x: x,
    y: y,
    i: i,
    j: j,
    w: w,
    h: h,
    ul: { x: x + w * cx, y: y + h * cy }, // upper left
    ur: { x: x + w * (cx + 1), y: y + h * cy }, //upper right
    center: { x: x + w * cx + w / 2, y: y + h * cy + h / 2 }, //center of square
    ll: { x: x + w * cx, y: y + h * (cy + 1) }, // lower left
    lr: { x: x + w * (cx + 1), y: y + h * (cy + 1) }, // lower right
    doorPercent: doorPercent
  };
}

function draw_maze(
  maze_pkg,
  x,
  y,
  w,
  h,
  ctx,
  wall_lineCap,
  wall_lineWidth,
  door_lineCap,
  door_lineWidth,
  doorPercent,
  key_font,
  door_font,
  border_thickness
) {
  var maze = maze_pkg.maze;
  draw_grid(ctx, x, y, w, h, maze_pkg.num_rows, maze_pkg.num_cols);
  draw_thick_border(ctx, x, y, w, h, maze_pkg.num_rows, maze_pkg.num_cols, border_thickness);

  var cellRow = true;
  for (var i in maze) {
    var cellCol = true;
    for (var j in maze[i]) {
      var coord = get_cell_coord(x, y, w, h, i, j, doorPercent);
      var cell = maze[i][j];

      if (cellRow & cellCol) {
        // Cell, draw text
        if (cell !== '') {
          draw_text(ctx, cell, coord.center.x, coord.center.y, key_font);
        }
      } else {
        if (cell === 'w') {
          // Wall
          if (cellRow) {
            // Vertical Wall
            draw_vert_wall(ctx, coord, wall_lineCap, wall_lineWidth);
          } else {
            //Horizontal Wall
            draw_hort_wall(ctx, coord, wall_lineCap, wall_lineWidth);
          }
        } else if (cell.startsWith('w')) {
          // Door
          if (cellRow) {
            // Vertical Door
            draw_vert_door(cell, ctx, coord, door_lineCap, door_lineWidth, door_font);
          } else {
            //Horizontal Door
            draw_hort_door(cell, ctx, coord, door_lineCap, door_lineWidth, door_font);
          }
        }
      }
      cellCol = !cellCol;
    }
    cellRow = !cellRow;
  }
}

function draw_thick_border(ctx, x, y, w, h, num_rows, num_cols, border_thickness) {
  ctx.beginPath();
  ctx.lineWidth = border_thickness;
  ctx.strokeStyle = 'black';
  ctx.lineCap = 'butt';

  ctx.moveTo(x, y);
  ctx.lineTo(x + num_cols * w, y);
  ctx.lineTo(x + num_cols * w, y + num_rows * h);
  ctx.lineTo(x, y + num_rows * h);
  ctx.lineTo(x, y);

  ctx.stroke();
}
function draw_grid(ctx, x, y, w, h, num_rows, num_cols) {
  ctx.beginPath();
  ctx.lineWidth = 1;
  ctx.strokeStyle = 'lightgrey';
  // draw vertical lines
  for (var vl = 0; vl <= num_cols; vl++) {
    ctx.moveTo(x + vl * w, y);
    ctx.lineTo(x + vl * w, y + num_rows * h);
  }
  // draw horizontal lines
  for (var hl = 0; hl <= num_rows; hl++) {
    ctx.moveTo(x, y + hl * h);
    ctx.lineTo(x + num_cols * w, y + hl * h);
  }
  ctx.stroke();
}
