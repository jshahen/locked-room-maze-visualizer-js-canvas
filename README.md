# README

Locked Room Maze viewer, using Javascript and HTML5 Canvas (no libraries).

## What is this repository for?

- Version v1

![screenshot of locked room maze](screenshots/maze.png 'Screenshot of Locked Room Maze')

A locked room maze is similar to a normal maze, except that going to an adjacent square is no longer always possible.
We now introduce the concept of **doors**, which only allow movement between adjacent squares if a condition is met.

For the simple examples shown in the screenshot, we use a concept of a **key** which unlocks all doors of the same
number/identifier as the key (i.e. obtaining key "16" unlocks all doors with the label "16").

To obtain a key, the player must create a path from the **Start** to the key, and for each door they pass through,
there must exists a porevious path to the corresponding key.

Key Concepts:

- Keys are obtained by landing on a space with large text on it
- Doors are indicated by walls with a gap and the key label written in smaller text
- Once a key is obtained you can use it as many times as you wish
- You must have the key before unlocking a door, thus backtracking the maze is not as easy

Advanced Concepts:

- The use of a hyphen in front of the key label denotes losing a key or a door which cannot be opened when a key is owned
- Doors can have more advance conditions
    - Doors that require multiple keys (ex: "16,20,4")
    - Doors that require a key not already be obtained (ex: "-20", can't have obtained the 20 key)
    - Combinations of the 2 (ex: "16,-20,4" - requires keys 16 and 4, AND requires not having key 20)

## How do I get set up?

- Download the file `locked-room-maze.js` and include it in your project

```html
<script src="locked-room-maze.js"></script>
```

## Copyright and License

- Copyright 2020 Jonathan Shahen
- MIT License
